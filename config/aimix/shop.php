<?php

return [
    
    'is_modifications_category' => false,
    'currency_default' => 'usd',
    'enable_in_stock' => false,
    
    // true = numeric, false = boolean
    'enable_in_stock_count' => false,
    
    // false = only base modification
    'enable_modifications' => true,

    // false = one image per product
    'enable_multiple_product_images' => true,
    
    'enable_complectations' => false,
    'enable_product_promotions' => false,
    'enable_is_hit' => true,
    'enable_brands' => false,
    'enable_attribute_groups' => false,
    'enable_attribute_icon' => false,
    'enable_product_rating' => false,
    'enable_product_category_pages' => false,
    'per_page' => 34,
    
];
