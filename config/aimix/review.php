<?php

return [
  'enable_review_type' => false,
  'enable_review_for_product' => true,
  'enable_rating' => false,
  'per_page' => 12,

];
