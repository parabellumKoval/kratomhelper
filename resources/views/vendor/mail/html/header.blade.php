<tr background="{{ url('/img/email-bg.png') }}">
    <td style="font: 16px Arial,sans-serif; line-height: 24px; color: #333333; padding: 20px 50px 45px; text-align: center;">
        <img border="0" width="72" height="16" style="max-width:72px; max-height: 16px; display: block; padding-bottom: 15px;" src="{{ url('/img/email-logo.png') }}" alt="Kratom">
        <b style="text-transform: uppercase; -webkit-text-size-adjust:none; font-size: 18px; margin: 0px; color: #ffffff; text-align: center;">{!! $slot !!}</b>
    </td>
</tr>

