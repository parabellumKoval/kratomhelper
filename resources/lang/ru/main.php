<?php

return [
  'cart_empty' => 'Your cart is empty',
  'order_success' => 'Your order is successfully placed',
  'form_error' => 'The form has not been submitted. Check that the fields are filled in correctly.',
  'review_success' => 'Your review has been successfully sent.',
];